package com.challenge.redis.controller;

import com.challenge.redis.dto.UserCreateDto;
import com.challenge.redis.dto.UserUpdateDto;
import com.challenge.redis.model.Constants;
import com.challenge.redis.model.User;
import com.challenge.redis.services.IUserService;
import io.reactivex.Completable;
import io.reactivex.Single;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private IUserService userService;

    @ApiOperation(value = Constants.API_OPERATION_GET_GETUSER_VALUE,
            httpMethod = Constants.API_OPERATION_GET_GETUSER_HTTPMETHOD,
            notes = Constants.API_OPERATION_GET_GETUSER_NOTES)
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Single<ResponseEntity<User>> getUser(@PathVariable String id) {
        return userService.getUser(id).map(ResponseEntity::ok);
    }

    @ApiOperation(value = Constants.API_OPERATION_GET_GETALLUSER_VALUE,
            httpMethod = Constants.API_OPERATION_GET_GETALLUSER_HTTPMETHOD,
            notes = Constants.API_OPERATION_GET_GETALLUSER_NOTES)
    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public Single<ResponseEntity<List<User>>> getAllUser() {
        return userService.getListUsers().map(ResponseEntity::ok);
    }

    @ApiOperation(value = Constants.API_OPERATION_POST_ADDUSER_VALUE,
            httpMethod = Constants.API_OPERATION_POST_ADDUSER_HTTPMETHOD,
            notes = Constants.API_OPERATION_POST_ADDUSER_NOTES)
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Single<ResponseEntity<User>> addUser(@RequestBody @Validated UserCreateDto userCreateDto) {
        return userService.addUser(userCreateDto).map(ResponseEntity::ok);
    }

    @ApiOperation(value = Constants.API_OPERATION_PUT_UPDATEUSER_VALUE,
            httpMethod = Constants.API_OPERATION_PUT_UPDATEUSER_HTTPMETHOD,
            notes = Constants.API_OPERATION_PUT_UPDATEUSER_NOTES)
    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public Completable updateUser(@RequestBody @Validated UserUpdateDto userUpdateDto) {
        return userService.updateUser(userUpdateDto);
    }

    @ApiOperation(value = Constants.API_OPERATION_DELETE_DELETEUSER_VALUE,
            httpMethod = Constants.API_OPERATION_DELETE_DELETEUSER_HTTPMETHOD,
            notes = Constants.API_OPERATION_DELETE_DELETEUSER_NOTES)
    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public Completable deleteUser(@PathVariable String id) {
        return userService.deleteUser(id);
    }

}
