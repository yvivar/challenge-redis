package com.challenge.redis.repository;

import com.challenge.redis.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, String> {
    @Query("SELECT u FROM user u WHERE u.id = :id and u.status = :status")
    User findByIdAndStatus(String id, String status);
}
