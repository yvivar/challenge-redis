package com.challenge.redis.exception;

import com.challenge.redis.model.Constants;
import io.swagger.annotations.ApiModel;

@ApiModel(value = Constants.API_EXCEPTION_USERNOTFOUNDEXCEPTION)
public class UserNotFoundException extends RuntimeException {
    public UserNotFoundException()  {
        super(Constants.USER_NOT_FOUND_EXCEPTION);
    }
}
