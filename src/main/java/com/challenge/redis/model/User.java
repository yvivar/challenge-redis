package com.challenge.redis.model;

import com.challenge.redis.dto.UserCreateDto;
import com.challenge.redis.dto.UserUpdateDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.Random;

@ApiModel(value = Constants.API_MODEL_USER)
@AllArgsConstructor
@Getter
@Setter
@Entity(name = Constants.MODEL_USER_ENTITY)
@Table(name = Constants.MODEL_USER_TABLE)
@RedisHash(Constants.REDIS_HASH)
public class User implements Serializable {

    @ApiModelProperty(position = Constants.API_MODEL_PROPERTY_USER_MODEL_ID_POSITION,
                      value = Constants.API_MODEL_PROPERTY_USER_MODEL_ID_VALUE,
                      dataType = Constants.API_MODEL_PROPERTY_USER_MODEL_ID_DATATYPE,
                      required = Constants.API_MODEL_PROPERTY_USER_MODEL_ID_REQUIRED)
    @NonNull
    @NotEmpty
    private String id;

    @ApiModelProperty(position = Constants.API_MODEL_PROPERTY_USER_MODEL_FIRST_NAME_POSITION,
                      value = Constants.API_MODEL_PROPERTY_USER_MODEL_FIRST_NAME_VALUE,
                      dataType = Constants.API_MODEL_PROPERTY_USER_MODEL_FIRST_NAME_DATATYPE,
                      required = Constants.API_MODEL_PROPERTY_USER_MODEL_FIRST_NAME_REQUIRED)
    @NotEmpty
    @NonNull
    private String firstName;

    @ApiModelProperty(position = Constants.API_MODEL_PROPERTY_USER_MODEL_LAST_NAME_POSITION,
                      value = Constants.API_MODEL_PROPERTY_USER_MODEL_LAST_NAME_VALUE,
                      dataType = Constants.API_MODEL_PROPERTY_USER_MODEL_LAST_NAME_DATATYPE,
                      required = Constants.API_MODEL_PROPERTY_USER_MODEL_LAST_NAME_REQUIRED)
    @NotEmpty
    @NonNull
    private String lastName;

    @ApiModelProperty(position = Constants.API_MODEL_PROPERTY_USER_MODEL_EMAILADDRESS_POSITION,
                      value = Constants.API_MODEL_PROPERTY_USER_MODEL_EMAILADDRESS_VALUE,
                      dataType = Constants.API_MODEL_PROPERTY_USER_MODEL_EMAILADDRESS_DATATYPE,
                      required = Constants.API_MODEL_PROPERTY_USER_MODEL_EMAILADDRESS_REQUIRED)
    @NotEmpty
    @NonNull
    private String emailAddress;

    @ApiModelProperty(position = Constants.API_MODEL_PROPERTY_USER_MODEL_STATUS_POSITION,
                      value = Constants.API_MODEL_PROPERTY_MODEL_MODEL_STATUS_VALUE,
                      dataType = Constants.API_MODEL_PROPERTY_MODEL_MODEL_STATUS_DATATYPE,
                      required = Constants.API_MODEL_PROPERTY_MODEL_MODEL_STATUS_REQUIRED)
    @NotEmpty
    @NonNull
    private String status;

    private static final long serialVersionUID = 1285454306356845811L;

    public static User getUserCreation(UserCreateDto userCreationDto) {
        return new User(String.valueOf(new Random().nextInt()),
                        userCreationDto.getFirstName(),
                        userCreationDto.getLastName(),
                        userCreationDto.getEmailAddress(),
                        Status.ACTIVE.toString());
    }

    public static User getUserUpdate(UserUpdateDto userUpdateDto) {
        return new User(userUpdateDto.getId(),
                        userUpdateDto.getFirstName(),
                        userUpdateDto.getLastName(),
                        userUpdateDto.getEmailAddress(),
                        Status.ACTIVE.toString());
    }
}
