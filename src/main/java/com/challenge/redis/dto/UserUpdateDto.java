package com.challenge.redis.dto;

import com.challenge.redis.model.Constants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@ApiModel(value = Constants.API_DTO_USERUPDATEDTO)
@Getter
@Setter
public class UserUpdateDto {

    @ApiModelProperty(position = Constants.API_MODEL_PROPERTY_USER_MODEL_ID_POSITION,
            value = Constants.API_MODEL_PROPERTY_USER_MODEL_ID_VALUE,
            dataType = Constants.API_MODEL_PROPERTY_USER_MODEL_ID_DATATYPE,
            required = Constants.API_MODEL_PROPERTY_USER_MODEL_ID_REQUIRED)
    @NonNull
    @NotEmpty
    private String id;

    @ApiModelProperty(position = Constants.API_MODEL_PROPERTY_USER_MODEL_FIRST_NAME_POSITION,
            value = Constants.API_MODEL_PROPERTY_USER_MODEL_FIRST_NAME_VALUE,
            dataType = Constants.API_MODEL_PROPERTY_USER_MODEL_FIRST_NAME_DATATYPE,
            required = Constants.API_MODEL_PROPERTY_USER_MODEL_FIRST_NAME_REQUIRED)
    @NotEmpty
    @NonNull
    private String firstName;

    @ApiModelProperty(position = Constants.API_MODEL_PROPERTY_USER_MODEL_LAST_NAME_POSITION,
            value = Constants.API_MODEL_PROPERTY_USER_MODEL_LAST_NAME_VALUE,
            dataType = Constants.API_MODEL_PROPERTY_USER_MODEL_LAST_NAME_DATATYPE,
            required = Constants.API_MODEL_PROPERTY_USER_MODEL_LAST_NAME_REQUIRED)
    @NotEmpty
    @NonNull
    private String lastName;

    @ApiModelProperty(position = Constants.API_MODEL_PROPERTY_USER_MODEL_EMAILADDRESS_POSITION,
            value = Constants.API_MODEL_PROPERTY_USER_MODEL_EMAILADDRESS_VALUE,
            dataType = Constants.API_MODEL_PROPERTY_USER_MODEL_EMAILADDRESS_DATATYPE,
            required = Constants.API_MODEL_PROPERTY_USER_MODEL_EMAILADDRESS_REQUIRED)
    @NotEmpty
    @NonNull
    private String emailAddress;

    @ApiModelProperty(position = Constants.API_MODEL_PROPERTY_USER_MODEL_STATUS_POSITION,
            value = Constants.API_MODEL_PROPERTY_MODEL_MODEL_STATUS_VALUE,
            dataType = Constants.API_MODEL_PROPERTY_MODEL_MODEL_STATUS_DATATYPE,
            required = Constants.API_MODEL_PROPERTY_MODEL_MODEL_STATUS_REQUIRED)
    @NotEmpty
    @NonNull
    private String status;
}
