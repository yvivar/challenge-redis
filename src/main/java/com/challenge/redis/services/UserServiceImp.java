package com.challenge.redis.services;

import com.challenge.redis.dto.UserCreateDto;
import com.challenge.redis.dto.UserUpdateDto;
import com.challenge.redis.exception.UserNotFoundException;
import com.challenge.redis.model.Status;
import com.challenge.redis.model.User;
import com.challenge.redis.repository.UserRepository;
import io.reactivex.Completable;
import io.reactivex.Single;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserServiceImp implements IUserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public Single<List<User>> getListUsers() {
        List<User> listUsers = new ArrayList<>();
        Iterable<User> iterableUser = emptyIfNull(this.userRepository.findAll());
        iterableUser.forEach(listUsers::add);
        return Single.just(
                listUsers.stream().filter(x -> Status.ACTIVE.toString().equals(x.getStatus())).collect(Collectors.toList())
        ).doOnError(x -> new UserNotFoundException());
    }

    @Override
    public Single<User> getUser(String id) {
        return Single.create(subscriber -> {
            Optional.ofNullable(this.userRepository.findByIdAndStatus(id, Status.ACTIVE.toString()))
                    .ifPresentOrElse(subscriber::onSuccess,
                            () -> subscriber.onError(new UserNotFoundException()));
        });
    }

    @Override
    public Single<User> addUser(UserCreateDto userCreateDto) {
        return Single.create(singleSubscriber -> singleSubscriber.onSuccess(
                this.userRepository.save(User.getUserCreation(userCreateDto))
        ));
    }

    @Override
    public Completable updateUser(UserUpdateDto userUpdateDto) {
        return Completable.create(subscriber ->
                this.userRepository.findById(userUpdateDto.getId()).ifPresentOrElse(
                        user -> {
                            user = User.getUserUpdate(userUpdateDto);
                            this.userRepository.save(user);
                            subscriber.onComplete();
                        },
                        () -> subscriber.onError(new UserNotFoundException())));
    }

    @Override
    public Completable deleteUser(String id) {
        return Completable.create(subscriber ->
                this.userRepository.findById(id).ifPresentOrElse(
                        user -> {
                            user.setStatus(Status.INACTIVE.toString());
                            this.userRepository.save(user);
                            subscriber.onComplete();
                        },
                        () -> subscriber.onError(new UserNotFoundException())));
    }

    public static <T> Iterable<T> emptyIfNull(Iterable<T> iterable) {
        return iterable == null ? Collections.<T>emptyList() : iterable;
    }

}
