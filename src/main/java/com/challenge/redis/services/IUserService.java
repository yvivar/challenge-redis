package com.challenge.redis.services;

import com.challenge.redis.dto.UserCreateDto;
import com.challenge.redis.dto.UserUpdateDto;
import com.challenge.redis.model.User;
import io.reactivex.Completable;
import io.reactivex.Single;

import java.util.List;

public interface IUserService {
    Single<List<User>> getListUsers();
    Single<User> getUser(String id);
    Single<User> addUser(UserCreateDto userCreateDto);
    Completable updateUser(UserUpdateDto userUpdateDto);
    Completable deleteUser(String id);
}
